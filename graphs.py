import asyncio
from httpx import AsyncClient

from plotting import get_figure, figure_to_png
from parser import STATUSES_RU, get_country_data


def plot(country_data):
    f = get_figure()
    axes = f.subplots(1)
    # axes.set_yscale('log')

    for status in STATUSES_RU:
        status_timeline = country_data['timeline'][status]
        dates = list(status_timeline.keys())
        cases = list(status_timeline.values())
        axes.plot(dates, cases, label=STATUSES_RU[status].capitalize())

    axes.grid()
    f.legend(loc='upper center', shadow=True, ncol=3)
    f.autofmt_xdate()

    return figure_to_png(f)


def rus_caption(rus_data):
    data_date = rus_data['last_updated'].date()
    lines = [f'Статистика COVID-19 в России на {data_date}:']
    for status in STATUSES_RU:
        status_name = STATUSES_RU[status].capitalize()
        status_cases = rus_data['latest'][status]
        lines.append(f'{status_name} – {status_cases}')
    lines.append(f'(по данным Johns Hopkins CSSE)')
    return '\n'.join(lines)


async def main():
    client = AsyncClient()

    rus_data = await get_country_data(client, 'Russia', timeline=True)
    
    with open("tmp/graph.png", "wb") as img:
        img.write(plot(rus_data))
    print(rus_caption(rus_data))

    await client.aclose()


if __name__ == '__main__':
    asyncio.run(main())
