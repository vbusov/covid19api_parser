import asyncio
from httpx import AsyncClient

import datetime as dt
from dateutil.parser import parse

API_URL = 'https://coronavirus-tracker-api.herokuapp.com/v2'


async def main():
    client = AsyncClient()
    rus_data = await get_country_data(client, 'Russia', timeline=True)
    
    data_date = rus_data['last_updated'].date()
    print(f'Данные на {data_date}: ')
    for status in STATUSES_RU:
        status_name = STATUSES_RU[status].capitalize()
        status_cases = rus_data['latest'][status]
        print(f'{status_name}: {status_cases}')
    
    for date in rus_data['timeline']['confirmed']:
        cases = rus_data['timeline']['confirmed'][date]
        if cases > 0:
            break
    first_case_date = date.date()
    print(f'Дата появления подтвержденных случаев в России: {first_case_date}')

    await client.aclose()


STATUSES_RU = {
    'confirmed': 'подтверждено',
    'recovered': 'выздоровело',
    'deaths': 'смертей'
}


async def get_country_data(client: AsyncClient, country, timeline=False):
    url = f'{API_URL}/locations'
    params = {'country': country}
    if timeline:
        params['timelines'] = 1
    r = await client.get(url, params=params)
    data = r.json()
    
    last_updated = parse(data['locations'][0]['last_updated'], ignoretz=True)
    case_data = {
        'last_updated': last_updated,
        'latest': {
            status: int(data['latest'][status]) 
            for status in STATUSES_RU
        }
    }
    if timeline:
        timelines = data['locations'][0]['timelines']
        case_data['timeline'] = {
            status: {
                parse(date, ignoretz=True): int(cases)
                for date, cases in timelines[status]['timeline'].items()
            } 
            for status in STATUSES_RU
        }

    return case_data


if __name__ == '__main__':
    asyncio.run(main())
